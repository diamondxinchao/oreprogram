﻿//Nhập số lượng quặng người chơi đã thu thập

Console.Write("Enter the amount of ore the player has collected: ");
int amountOfOre = int.Parse(Console.ReadLine());

/*
Lấy 10 quặng đầu tiên trong tổng số quặng
Tính tiền 10 quặng đầu tiền
Tính số quặng còn lại
*/
int tenOreFirst = Math.Min(amountOfOre, 10);
int goldOfTenOreFirst = tenOreFirst * 10;
amountOfOre -= tenOreFirst;
Console.WriteLine("Gold of ten ore first you received: " + tenOreFirst);
/*
Lấy 5 quặng tiếp theo trong tổng số quặng còn lại
Tính tiền 5 quặng đó
Tính số quặng còn lại
*/
int fiveOreNext = Math.Min(amountOfOre, 5);
int goldOfFiveOreNext = fiveOreNext * 5;
amountOfOre -= fiveOreNext;
Console.WriteLine("Gold of five ore next you received: " + fiveOreNext);
/*
Lấy 3 quặng tiếp theo trong tổng số quặng còn lại
Tính tiền 3 quặng đó
Tính số quặng còn lại
*/
int threeOreNext = Math.Min(amountOfOre, 3);
int goldOfThreeOreNext = threeOreNext * 2;
amountOfOre -= threeOreNext;
Console.WriteLine("Gold of three ore next you received: " + threeOreNext);
//Tính tiền dựa trên số quặng còn lại
int remainGold = amountOfOre * 1;
Console.WriteLine("Remain gold you received: " + remainGold);
//Tổng tất cả tiền dựa trên số quặng bạn thu thập được
int totalPrice = (goldOfTenOreFirst + goldOfFiveOreNext + goldOfThreeOreNext + remainGold);
Console.WriteLine("Total gold that you received is: " + totalPrice);